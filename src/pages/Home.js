import Banner from '../components/Banner'
import Highlights from '../components/Highlights';


export default function Home() {
    return(

        <>
            <Banner name="John Michael" age={53} />
            <Highlights/>
            
        
        </>
        )
}
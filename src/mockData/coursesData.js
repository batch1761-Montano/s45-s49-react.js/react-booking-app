const coursesData = [
    {
        id: "wdc001",
        name: "PHP - Laravel",
        description: "Php - Laravel Course",
        price: 45000
    },

    {
        id: "wdc002",
        name: "Python - Django",
        description: "Python - Django Course",
        price: 55000
    },

    {
        id: "wdc003",
        name: "Java - Springbbot",
        description: "Java- Springboot Course",
        price: 65000
    }

]

export default coursesData;